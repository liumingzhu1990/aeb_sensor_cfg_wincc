﻿#pragma once


// mmw_radar_display 对话框

class mmw_radar_display : public CDialogEx
{
	DECLARE_DYNAMIC(mmw_radar_display)

public:
	mmw_radar_display(CWnd* pParent = nullptr);   // 标准构造函数
	virtual ~mmw_radar_display();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DLG_WWM_DIS };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
private:
	CImage image;		// 创建图片类 
	CRect rect;			// 定义矩形类 
	void LoadPicture(void);

public:
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
};
